﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ChatApp.Signalar.Domain
{
    public class User
    {
        [Key]
        public long Id { get; set; }
        [Required]
        public string FullName { get; set; }
        public string UserName { get; set; }
        [Required]
        public string PassWord { get; set; }
        [Required]
        public string Email { get; set; }
        public long? RoleId { get; set; }

    }
}
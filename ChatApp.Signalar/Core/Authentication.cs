﻿using ChatApp.Signalar.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Web;
using System.Web.Security;

namespace ChatApp.Signalar.Core
{
    public static class Authentication
    {
        public static void SaveAuthenticationInformation(long userId, string email, string role, bool rememberMe, string name)
        {
            SaveAuthenticationInformation(userId, email, new List<string> { role }, rememberMe, name);
        }
        public static void SaveAuthenticationInformation(long userId, string email, List<string> roles, bool rememberMe, string name)
        {
            if (HttpContext.Current == null)
                throw new ArgumentNullException();

            var strRoles = new StringBuilder();
            roles.ForEach(x => strRoles.Append(x + "|"));
            var allRoles = strRoles.ToString().Remove(strRoles.ToString().LastIndexOf("|"), 1);

            var authTicket = new FormsAuthenticationTicket(
                1,
                name,
                DateTime.UtcNow,
                rememberMe ? DateTime.UtcNow.AddYears(1) : DateTime.UtcNow.AddMinutes(60),
                rememberMe,
                userId + "|" + email + "|"  + allRoles + "|" + name);

            var encryptedTicket = FormsAuthentication.Encrypt(authTicket);
            var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);

            HttpContext.Current.Response.Cookies.Add(authCookie);
        }

        [SecurityPermission(SecurityAction.Demand, ControlPrincipal = true)]
        public static void LoadAuthenticationInformation()
        {
            if (HttpContext.Current.User != null && (HttpContext.Current.User is UserPrincipal)) return;

            var authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie == null) return;

            var authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            if (authTicket == null) return;

            var identity = new FormsIdentity(authTicket);

            var userId = authTicket.UserData.Split('|')[0];
            var email = authTicket.UserData.Split('|')[1];
            var fullName = authTicket.UserData.Split('|')[3];
            var roles = authTicket.UserData.Split('|').Where((val, index) => index != 0 && index != 1 && index != 3).ToArray();
            HttpContext.Current.User = new UserPrincipal(identity, userId, email, roles, fullName);
        }
    }
}
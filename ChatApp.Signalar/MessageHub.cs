﻿using ChatApp.Signalar.Helper;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatApp.Signalar
{
    public class MessageHub:Hub
    {
        //private IHubContext hubContext { get; set; }
        //public MessageHub()
        //{
        //    hubContext = GlobalHost.ConnectionManager.GetHubContext<MessageHub>();
        //}

        public void SendMessage(string userId,string Message)
        {
            Clients.User(userId).addNewMessage(userId, Message);
        }

    }

    public class UserIdProvider : IUserIdProvider
    {
        public string GetUserId(IRequest request)
        {
            string userid = string.Empty;
            if (request != null && request.User != null && request.User is UserPrincipal)
            {
                var userPrincipal = (UserPrincipal)request.User;
                if (userPrincipal != null)
                {
                    userid = userPrincipal.UserId.ToString();
                }
            }
            return userid;
        }
    }
}
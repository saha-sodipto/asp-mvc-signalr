﻿namespace ChatApp.Signalar.Migrations
{
    using ChatApp.Signalar.Domain;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ChatApp.Signalar.DbContextSetting.WebSocketMessageingDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ChatApp.Signalar.DbContextSetting.WebSocketMessageingDbContext context)
        {
            var users = new List<User>
                    {
                        new User()
                        {
                            Email="s@gmail.com",
                            FullName="Sodipto Saha",
                            UserName="saha",
                            PassWord="123456",

                        },
                        new User()
                        {
                            Email="b@gmail.com",
                            FullName="Bithy Saha",
                            UserName="bithy",
                            PassWord="123456",
                        },
                    };

            context.SaveChanges();
        }
    }
}

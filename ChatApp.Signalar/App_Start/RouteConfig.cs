﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ChatApp.Signalar
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //Authentication
            routes.MapRoute(
                name: "Login",
                url: "log-in",
                defaults: new { controller = "Home", action = "Login", id = UrlParameter.Optional }
            );

            //Message
            routes.MapRoute(
                 name: "Message",
                 url: "",
                 defaults: new { controller = "Message", action = "ChatDashboard" }
             );
        }
    }
}

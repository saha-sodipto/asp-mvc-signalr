﻿using ChatApp.Signalar.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatApp.Signalar.Helper
{
    public static class HttpContextHelper
    {
        public static bool IsAuthenticated
        {
            get { return HttpContext.Current.User.Identity.IsAuthenticated; }
        }
      
        public static UserPrincipal Current
        {
            get
            {
                if (HttpContext.Current == null)
                    throw new ArgumentNullException("HttpContext.Current", "Currently no user attached with this session.");

                return HttpContext.Current.User as UserPrincipal;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace ChatApp.Signalar.Helper
{
    public sealed class UserPrincipal : GenericPrincipal
    {
        public long UserId { get; private set; }
        public string FullName { get; private set; }
        public string UserEmail { get; private set; }

        public UserPrincipal(IIdentity identity,string userId, string email, string[] roles, string fullName)
            : base(identity, roles)
        {
            UserId =Convert.ToInt64(userId);
            FullName = fullName;
            UserEmail = email;
        }
    }
}
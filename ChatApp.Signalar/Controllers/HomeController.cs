﻿using ChatApp.Signalar.Core;
using ChatApp.Signalar.DbContextSetting;
using ChatApp.Signalar.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChatApp.Signalar.Controllers
{
    public class HomeController : Controller
    {
        protected WebSocketMessageingDbContext _context;
        public HomeController()
        {
            _context = new WebSocketMessageingDbContext();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View("~/Views/Home/Login.cshtml",new LoginViewModel());
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            var user = _context.Users.Where(x => x.Email == model.Email && x.PassWord == model.Password).FirstOrDefault();
            if (user==null)
            {
                ViewBag.ErrorMessage = "Wrong password or username";
                return View("~/Views/Home/Login.cshtml");
            }
            else
            {
                Authentication.SaveAuthenticationInformation(user.Id,user.Email,"Nuser",model.IsRemember,user.FullName);
                return RedirectToRoute("Message");
            }
            
        }
    }
}
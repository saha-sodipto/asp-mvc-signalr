﻿using ChatApp.Signalar.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChatApp.Signalar.Controllers
{
    [Authorize]
    public class MessageController : Controller
    {
        // GET: Message
        public ActionResult ChatDashboard()
        {
            ViewBag.ReceiverId = HttpContextHelper.Current.UserId == 1 ? 2 : 1;
            return View();
        }
    }
}